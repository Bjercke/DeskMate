$(document).ready(function() {

    // Opening the left menu
    var winWidth = $(window).width();
    function menuToggler() {
        $('.leftMenu').toggle('slide', {
            direction : 'left'
        }, 500);
        $('.openLeftMenu').toggle('fade', 500);
    }
    if (winWidth > 1100) {
        menuToggler();
    }
    $('.openLeftMenu, .leftMenuTopBtn').click(function() {
       menuToggler();
   });

    var newBoxContent = '<div class="dragBox-wrapper"><div class="dragBox-top-bar"><div class="dragBox-top-bar-title">Notat</div><div class="dragBox-close-box"><span>X</span></div></div><div class="dragBox"></div></div>';

    // Spawn a new box on click
    $('.makeNewBox').click(function() {
        $('.windows').append(newBoxContent);

        // Make boxes draggable
        $('.dragBox-wrapper').draggable({
            handle: ".dragBox-top-bar",
            containment: "window"
        });
        $('.dragBox-close-box').disableSelection();
    });

    setInterval(function() {
        var date = new Date();
        $('.spawn-clock').html(
            date.getHours() + ":" + date.getMinutes()
        );
    }, 500);

});