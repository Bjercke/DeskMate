<footer>
    <div class="footer-content">
        <div class="bottom-logo"><span class="bottom-logo-title"><?= $lang->get('developed_by') ?></span><img src="img/logo.png" alt="Bjercke Media"></div>
    </div>
</footer>

<script src="dist/bundle.js"></script>
</body>
</html>