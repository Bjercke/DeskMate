<?php

class Language {

    public $returnInLanguage;

    public function __construct($language) {
        if ($language == 'noNO') {
            $this->returnInLanguage = 'norwegian';
        } elseif ($language == 'enEN') {
            $this->returnInLanguage = 'english';
        }
    }

    public function get($langstring)
    {
        global $sqlConnect;

        $query = "SELECT * FROM language WHERE langstring='$langstring'";
        $result = $sqlConnect->query($query);
        $assoc = $result->fetch_assoc();
        if ($result->num_rows > 0) {
            if ($this->returnInLanguage == 'norwegian') {
                return $assoc['no'];
            } else {
                return $assoc['en'];
            }
        } else {
            return false;
        }

    }

}