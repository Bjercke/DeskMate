**Basic login and UI by Bjercke Media**
<br />Made for making new projects faster and easier.
- You can find an .sql file in the "INSTALL" folder - then apply correct settings to the config/config.php file.
- Make sure you have <a href="https://nodejs.org">Node.js</a> installed
- Start by typing "npm install" in your terminal
- Check out webpack.config.js for scripts