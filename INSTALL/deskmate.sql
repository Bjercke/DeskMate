-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 04, 2018 at 09:38 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `deskmate`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `name`, `email`, `password`, `level`) VALUES
(1, 'Marius Bjercke', 'marius@bjerckemedia.no', 'Desktopdesk94', 1),
(2, 'Victoria Bjercke', 'victoria@bjerckemedia.no', 'Rosadesk951', 0),
(3, 'Stein Bjercke', 'sb@test.no', 'passord', 0);

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `id` int(11) NOT NULL,
  `sitename` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL DEFAULT 'noNO',
  `maintenance` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`id`, `sitename`, `language`, `maintenance`) VALUES
(1, 'Min nettside', 'noNO', 0);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) NOT NULL,
  `langstring` varchar(255) NOT NULL,
  `no` text,
  `en` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Adding language';

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `langstring`, `no`, `en`) VALUES
(1, 'note', 'Notat', 'Note'),
(2, 'menu', 'Meny', 'Menu'),
(3, 'add_note', 'Lag notat', 'Add note'),
(4, 'settings', 'Instillinger', 'Settings'),
(5, 'logout', 'Logg ut', 'Logout'),
(6, 'notes', 'Notater', 'Notes'),
(7, 'home', 'Hjem', 'Home'),
(9, 'customers', 'Kunder', 'Customers'),
(10, 'addressbook', 'Adresseliste', 'Address book'),
(11, 'news', 'Nyheter', 'News'),
(12, 'signin', 'Logg inn', 'Sign in'),
(13, 'email', 'E-post', 'Email'),
(14, 'emailaddress', 'E-post adresse', 'Email address'),
(15, 'rememberme', 'Husk meg', 'Remember me?'),
(16, 'forgotpassword', 'Glemt passord?', 'Forgot the password?'),
(17, 'password', 'Passord', 'Password'),
(18, 'maintenance', 'Nettsiden er nede grunnet vedlikehold.', 'The site is down for maintenance.'),
(19, 'login_error', 'Noe gikk galt, vennligst prøv igjen.', 'Something went wrong, please try again.'),
(20, 'new_customer', 'Ny kunde?', 'New customer?'),
(21, 'loremipsum', 'Lorem ipsum', 'Lorem ipsum'),
(22, 'developed_by', 'Utviklet av', 'Developed by');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
