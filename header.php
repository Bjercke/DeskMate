<?php
session_start();
include 'config/config.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= $siteName ?> v1.1 | Bjercke Media</title>
    <link rel="stylesheet" href="dist/main.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<div class="leftMenu">
    <div class="leftMenuTop">
        <div class="leftMenuTopBtn"><i class="fa fa-bars"></i></div>
        <span class="title"><?= $lang->get('menu') ?></span>
    </div>
</div>
<header>
    <div class="top-left-menu">
        <div class="top-left-menu-content">
            <ul>
                <li class="openLeftMenu"><i class="fa fa-bars"></i></li>
            </ul>
        </div>
    </div>

    <div class="top-right-menu">
        <ul>
            <a href="#"><li class="header-profile-dropdown"><span class="topName"><?= $account->fullName ?></span><div class="topUserIcon"></div> <i class="fa fa-caret-down"></i></li></a>
            <a href="#"><li><i class="fa fa-cog"></i></li></a>
            <li class="power-off-icon"><a href="logout.php" title="<?= $lang->get('logout') ?>"><i class="fa fa-power-off"></i></a></li>
            <li class="li-clock">
                <div class="top-right-clock"><i class="fa fa-clock-o"></i> <span class="spawn-clock"></span></div>
            </li>
        </ul>
    </div>
</header>