<?php
// Check if a user is logged in
$current_file_name = basename($_SERVER['PHP_SELF']);
if (isset($_SESSION['acc_logged_in'])) {
    if ($_SESSION['acc_logged_in'] == true) {
        if ($current_file_name == 'login.php') {
            header("Location: index.php");
        }
    } else {
        if ($current_file_name != 'login.php') {
            header("Location: login.php");
        }
    }
} else {
    if ($current_file_name != 'login.php') {
        header("Location: login.php");
    }
}

// Connect to MySQL
// Connection details
$host = '127.0.0.1';
$username = 'root';
$password = 'Desktopsql94';
$database = 'deskmate';
$sqlConnect = new mysqli($host, $username, $password, $database);
mysqli_set_charset($sqlConnect, "utf8");

if ($sqlConnect->connect_error) {
    echo 'Error connecting to the database.';
    exit;
}

// Include classes underneath. Remember; the path is being read as from root.
include 'includes/classes/language.php';
include 'includes/classes/account.php';

// General site config
$configQuery = "SELECT * FROM config WHERE id='1'";
$configResult = $sqlConnect->query($configQuery);
$configAssoc = $configResult->fetch_assoc();

// Site name
$siteName = $configAssoc['sitename'];

// Site language, fetched from DB
// 'noNO' for Norwegian
// 'enEN' for English
$language = $configAssoc['language'];
$lang = new Language($language);

// Is the site in maintenance mode? If so, show error message.
$maintenance = $configAssoc['maintenance'];
if ($maintenance > 0) {
    echo $lang->get('maintenance');
    exit;
}


// Retrieve signed in account
if (isset($_SESSION['acc_id'])) {
    $accID = $_SESSION['acc_id'];
    $account = new Account();
    $account->getAccByID($accID);
}
global $account;