<?php
session_start();
include 'config/config.php';

// TODO: Add sanitizers
if (isset($_POST['submit'])){
    $email = $_POST['email'];
    $password = $_POST['password'];

    $account = new Account();
    $result = $account->authAcc($email, $password);

    if ($result) {
        $_SESSION['acc_logged_in'] = true;
        $_SESSION['acc_id'] = $account->accID;
        echo '<script>window.location="index.php?login=success"</script>';
    } else {
        echo '<script>window.location="login.php?msg=error1"</script>';
        exit;
    }
}

?>
<!DOCTYPE html>
<html lang="en" class="loginBodyHtml">
<head>
    <meta charset="UTF-8">
    <title><?= $lang->get('signin') ?> | <?= $siteName ?></title>
    <link rel="stylesheet" href="dist/main.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body class="loginBodyHtml">

<div class="login">

    <div class="container">
        <div class="card card-container">
            <h6 class="login-title"><?= $lang->get('loremipsum') ?></h6>
            <h4 class="login-second-title"><?= $lang->get('loremipsum') ?></h4>
            <form class="form-signin" method="post" action="">
                <span id="reauth-email" class="reauth-email"></span>
                <input type="email" name="email" id="inputEmail" class="form-control" placeholder="<?= $lang->get('emailaddress') ?>" required autofocus>
                <input type="password" name="password" id="inputPassword" class="form-control" placeholder="<?= $lang->get('password') ?>" required>

                <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" name="remember" value="remember-me"> <?= $lang->get('rememberme') ?>
                    </label>
                </div>
                <?php
                if (isset($_GET['msg'])) {
                    if ($_GET['msg'] == 'error1') {
                        echo '<p class="errorMsg">'.$lang->get('login_error').'</p>';
                    }
                }
                ?>
                <button class="btn btn-lg btn-primary btn-block btn-signin" name="submit" type="submit"><?= $lang->get('signin') ?></button>
            </form><!-- /form -->
            <a href="#" class="forgot-password">
                <?= $lang->get('loremipsum') ?>
            </a>
        </div>
    </div>

</div>

<script src="dist/bundle.js"></script>
</body>
</html>